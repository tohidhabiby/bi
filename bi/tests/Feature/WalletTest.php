<?php

namespace Tests\Feature;

use App\Models\Accounting\Transaction;
use App\Models\Accounting\Wallet;
use App\Models\User\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class WalletTest extends TestCase
{
    use WithFaker, RefreshDatabase;
    /**
     * @test
     */
    public function userCanCreateWallet()
    {
        $user = User::factory()->create();
        $name = $this->faker->word;
        $response = $this->postJson(
            route('wallets.store'),
            [
                Wallet::USER_ID => $user->getId(),
                Wallet::NAME => $name,
            ]
        )->assertCreated();
        $this->assertEquals($response->getOriginalContent()->getUserId(), $user->getId());
        $this->assertEquals($response->getOriginalContent()->getName(), $name);
    }

    /**
     * @test
     */
    public function userCanGetAWallet()
    {
        $wallet = Wallet::factory()->create();
        $response = $this->getJson(route('wallets.show', $wallet))->assertOk();
        $this->assertTrue($response->getOriginalContent()->is($wallet));
    }

    /**
     * @test
     */
    public function userCanDoTransactions()
    {
        $wallet = Wallet::factory()->create();
        $amount = $this->faker->numberBetween(1, 1000000000);
        $response = $this->postJson(
            route('transaction', $wallet),
            [
                Transaction::IS_DEPOSIT => true,
                Transaction::AMOUNT => $amount,
            ]
        );
        $this->assertEquals($response->getOriginalContent()->getAmount(), $amount);
        $transaction = $wallet->transactions()->latest()->first();
        $this->assertEquals($transaction->getAmount(), $amount);
        $this->assertEquals($transaction->getBeforeAmount(), 0);
        $this->assertEquals($transaction->getAfterAmount(), $amount);
        $response = $this->postJson(
            route('transaction', $wallet),
            [
                Transaction::IS_DEPOSIT => false,
                Transaction::AMOUNT => $amount,
            ]
        );
        $this->assertEquals($response->getOriginalContent()->getAmount(), 0);
        $transaction = $wallet->transactions()->latest()->first();
        $this->assertEquals($transaction->getAmount(), -$amount);
        $this->assertEquals($transaction->getBeforeAmount(), $amount);
        $this->assertEquals($transaction->getAfterAmount(), 0);
    }

    /**
     * @test
     */
    public function userCanNotWithdrawMoreThanRestAmount()
    {
        $transaction = Transaction::factory()->create([Transaction::IS_DEPOSIT => true]);
        $response = $this->postJson(
            route('transaction', $transaction->wallet),
            [
                Transaction::IS_DEPOSIT => false,
                Transaction::AMOUNT => $transaction->getAfterAmount() + 1,
            ]
        );

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        /** @var  array $content */
        $content = $response->json()['errors'];
        $this->assertArrayHasKey(Transaction::AMOUNT, $content);
    }

    /**
     * @return void
     */
    public function userCanNotCreateWalletWithWrongData()
    {
        $response = $this->postJson(route('wallets.store'), []);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        /** @var  array $content */
        $content = $response->getOriginalContent()->toArray();
        $this->assertArrayHasKey(Wallet::NAME, $content);
        $this->assertArrayHasKey(Wallet::USER_ID, $content);
        $response = $this->postJson(
            route('wallets.store'),
            [
                Wallet::USER_ID => $this->faker->numberBetween(100, 1000),
                Wallet::NAME => true
            ]
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        /** @var  array $content */
        $content = $response->getOriginalContent()->toArray();
        $this->assertArrayHasKey(Wallet::NAME, $content);
        $this->assertArrayHasKey(Wallet::USER_ID, $content);
        $response = $this->postJson(
            route('wallets.store'),
            [
                Wallet::USER_ID => 'test',
                Wallet::NAME => 123
            ]
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        /** @var  array $content */
        $content = $response->getOriginalContent()->toArray();
        $this->assertArrayHasKey(Wallet::NAME, $content);
        $this->assertArrayHasKey(Wallet::USER_ID, $content);
    }

    /**
     * @test
     */
    public function userCanTransfer()
    {
        $sourceWallet = Wallet::factory()->create();
        $targetWallet = Wallet::factory()->create();
        Transaction::factory()->create(
            [
                Transaction::AMOUNT => 100000,
                Transaction::WALLET_ID => $sourceWallet->getId(),
                Transaction::IS_DEPOSIT => true,
                Transaction::BEFORE_AMOUNT => 0,
                Transaction::AFTER_AMOUNT => 100000,
            ]
        );
        $response = $this->postJson(
            route('transfer', $sourceWallet),
            [
                'target_id' => $targetWallet->getId(),
                Transaction::AMOUNT => 10000,
            ]
        )->assertOk();
        $this->assertEquals($sourceWallet->refresh()->getAmount(), 89850);
        $this->assertEquals($targetWallet->refresh()->getAmount(), 10000);
        $this->assertDatabaseHas(
            Transaction::TABLE,
            [
                Transaction::AMOUNT => 10000,
                Transaction::WALLET_ID => $targetWallet->getId(),
                Transaction::TYPE => Transaction::TYPE_USER,
            ]
        );
        $this->assertDatabaseHas(
            Transaction::TABLE,
            [
                Transaction::AMOUNT => -10000,
                Transaction::WALLET_ID => $sourceWallet->getId(),
                Transaction::TYPE => Transaction::TYPE_USER,
            ]
        );
        $this->assertDatabaseHas(
            Transaction::TABLE,
            [
                Transaction::AMOUNT => -150,
                Transaction::WALLET_ID => $sourceWallet->getId(),
                Transaction::TYPE => Transaction::TYPE_COMMISSION,
            ]
        );
    }

    /**
     * @test
     */
    public function userCanNotTransferMoreThanOrEqualBalance()
    {
        $sourceWallet = Wallet::factory()->create();
        $targetWallet = Wallet::factory()->create();
        Transaction::factory()->create(
            [
                Transaction::AMOUNT => 10000,
                Transaction::WALLET_ID => $sourceWallet->getId(),
                Transaction::IS_DEPOSIT => true,
                Transaction::BEFORE_AMOUNT => 0,
                Transaction::AFTER_AMOUNT => 10000,
            ]
        );
        $response = $this->postJson(
            route('transfer', $sourceWallet),
            [
                'target_id' => $targetWallet->getId(),
                Transaction::AMOUNT => 10000,
            ]
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        /** @var  array $content */
        $content = $response->getOriginalContent()['errors'];
        $this->assertArrayHasKey(Transaction::AMOUNT, $content);
    }
}
