<?php

namespace Database\Factories\Accounting;

use App\Models\Accounting\Transaction;
use App\Models\Accounting\Wallet;
use Illuminate\Database\Eloquent\Factories\Factory;

class TransactionFactory extends Factory
{
    protected $model = Transaction::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $amount = $this->faker->numberBetween(1, 1000000000);

        return [
            Transaction::AMOUNT => $amount,
            Transaction::WALLET_ID => Wallet::factory(),
            Transaction::IS_DEPOSIT => $this->faker->boolean,
            Transaction::BEFORE_AMOUNT => 0,
            Transaction::AFTER_AMOUNT => $amount,
            Transaction::TYPE => $this->faker->randomElement(Transaction::$types),
        ];
    }
}
