<?php

use App\Models\Accounting\Transaction;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Transaction::TABLE, function (Blueprint $table) {
            $table->enum(Transaction::TYPE, Transaction::$types)
                ->default(Transaction::TYPE_USER)
                ->comment(implode(Transaction::$types));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Transaction::TABLE, function (Blueprint $table) {
            $table->dropColumn([Transaction::TYPE]);
        });
    }
}
