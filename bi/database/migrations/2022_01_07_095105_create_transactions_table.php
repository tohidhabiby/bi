<?php

use App\Models\Accounting\Transaction;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Transaction::TABLE, function (Blueprint $table) {
            $table->id();
            $table->foreignId(Transaction::WALLET_ID)->constrained();
            $table->bigInteger(Transaction::AMOUNT);
            $table->bigInteger(Transaction::BEFORE_AMOUNT);
            $table->bigInteger(Transaction::AFTER_AMOUNT);
            $table->boolean(Transaction::IS_DEPOSIT);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Transaction::TABLE);
    }
}
