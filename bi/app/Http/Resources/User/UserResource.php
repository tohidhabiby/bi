<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Wallet\WalletResource;
use App\Models\User\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request): array
    {
        return [
            User::ID => $this->getId(),
            User::EMAIL => $this->getEmail(),
            User::NAME => $this->getName(),
            'wallets' => WalletResource::collection($this->wallets),
        ];
    }
}
