<?php

namespace App\Http\Resources\Wallet;

use App\Models\Accounting\Wallet;
use Illuminate\Http\Resources\Json\JsonResource;

class WalletResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request): array
    {
        return [
            Wallet::ID => $this->getId(),
            Wallet::USER_ID => $this->getUserId(),
            Wallet::NAME => $this->getName(),
            Wallet::AMOUNT => $this->getAmount(),
            'transactions' => $this->whenLoaded(
                'transactions',
                function () {
                    return TransactionResource::collection($this->transactions);
                }
            ),
        ];
    }
}
