<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class TransactionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $key = 'wallet_' . explode('/', $request->getPathInfo())[3];
        $array = [];
        if (Cache::has($key)) {
            $array = Cache::get($key);
        }
        $array[] = time();
        Cache::put($key, $array);
        if (!empty($array)) {
            sleep(1);
        }

        return $next($request);
    }
}
