<?php

namespace App\Http\Controllers;

use App\Filters\WalletFilter;
use App\Http\Requests\Wallet\TransactionRequest;
use App\Http\Requests\Wallet\WalletRequest;
use App\Http\Requests\Wallet\WalletTransferRequest;
use App\Http\Resources\Wallet\WalletResource;
use App\Models\Accounting\Transaction;
use App\Models\Accounting\Wallet;
use App\Models\User\User;
use App\Repositories\WalletRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class WalletController extends Controller
{
    /**
     * @param WalletFilter $filters
     * @return AnonymousResourceCollection
     */
    public function index(WalletFilter $filters): AnonymousResourceCollection
    {
        return WalletResource::collection(Wallet::filter($filters)->paginate());
    }

    /**
     * @param Wallet $wallet
     * @return WalletResource
     */
    public function show(Wallet $wallet): WalletResource
    {
        return new WalletResource($wallet->load('transactions'));
    }

    /**
     * @param WalletRequest $request
     * @return WalletResource
     */
    public function store(WalletRequest $request): WalletResource
    {
        $user = User::find($request->get(Wallet::USER_ID));

        return new WalletResource(Wallet::createObject($user, $request->get(Wallet::NAME)));
    }

    /**
     * @param TransactionRequest $request
     * @param Wallet $wallet
     * @param WalletRepository $repository
     *
     * @return WalletResource
     */
    public function transaction(
        TransactionRequest $request,
        Wallet $wallet,
        WalletRepository $repository
    ): WalletResource {
        return new WalletResource(
            $repository->doTransaction(
                $wallet,
                $request->get(Transaction::AMOUNT),
                $request->get(Transaction::IS_DEPOSIT)
            )
        );
    }

    /**
     * @param WalletTransferRequest $request
     * @param Wallet $wallet
     * @param WalletRepository $repository
     * @return WalletResource|JsonResponse
     */
    public function transfer(
        WalletTransferRequest $request,
        Wallet $wallet,
        WalletRepository $repository
    ): WalletResource|JsonResponse
    {
        $response = $repository->transfer(
            $wallet,
            $request->get('target_id'),
            $request->get(Transaction::AMOUNT)
        );
        if ($response instanceof Wallet) {
            return new WalletResource($wallet);
        }

        return $this->getResponse(['message' => $response['message']], $response['status']);
    }
}
