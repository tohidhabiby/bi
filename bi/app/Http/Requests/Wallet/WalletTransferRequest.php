<?php

namespace App\Http\Requests\Wallet;

use App\Models\Accounting\Transaction;
use App\Models\Accounting\Wallet;
use App\Rules\CheckWithdrawRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class WalletTransferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'target_id' => ['required', 'integer', Rule::exists(Wallet::TABLE, Wallet::ID)],
            Transaction::AMOUNT => [
                'required',
                'integer',
                'min:0',
                new CheckWithdrawRule($this->wallet, false, true),
            ],
        ];
    }
}
