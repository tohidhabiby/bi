<?php

namespace App\Repositories;

use App\Models\Accounting\Transaction;
use App\Models\Accounting\Wallet;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class WalletRepository
{
    /**
     * @param Wallet $wallet
     * @param int $amount
     * @param bool $isDeposit
     * @return Wallet
     */
    public function doTransaction(Wallet $wallet, int $amount, bool $isDeposit): Wallet
    {
        $wallet->doTransaction($amount, $isDeposit);
        $this->popCache($wallet);

        return $wallet;
    }

    /**
     * @param Wallet $wallet
     * @param int $targetId
     * @param int $amount
     * @return Wallet|array
     */
    public function transfer(Wallet $wallet, int $targetId, int $amount): Wallet|array
    {
        // the commission percentage is 0.015, you can read it from env or config or database
        DB::beginTransaction();
        try {
            $targetWallet = Wallet::findOrFail($targetId);
            $wallet->doTransaction($amount, false);
            $wallet->doTransaction($amount * 0.015, false, Transaction::TYPE_COMMISSION);
            $targetWallet->doTransaction($amount, true);
            $this->popCache($wallet);
            DB::commit();

            return $wallet;
        } catch (\Exception $exception) {
            DB::rollBack();

            Log::error(
                'Transfer Error: ' . $exception->getMessage(),
                ['source' => $wallet, 'amount' => $amount, 'target_id' => $targetId]);

            return [
                'message' => 'There is an error! Please contact support.',
                'status' => Response::HTTP_BAD_REQUEST,
            ];
        }
    }

    /**
     * @param Wallet $wallet
     * @return void
     */
    private function popCache(Wallet $wallet) {
        $key = 'wallet_' . $wallet->getId();
        $array = Cache::get($key);
        unset($array[key($array)]);
        Cache::put($key, $array);
    }
}
