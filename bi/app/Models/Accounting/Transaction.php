<?php

namespace App\Models\Accounting;

use Habibi\Models\BaseModel;
use Habibi\Traits\HasAmountTrait;
use Habibi\Traits\HasWalletIdTrait;

class Transaction extends BaseModel
{
    use HasAmountTrait;
    use HasWalletIdTrait;

    const TABLE = 'transactions';
    const WALLET_ID = 'wallet_id';
    const AMOUNT = 'amount';
    const BEFORE_AMOUNT = 'before_amount';
    const AFTER_AMOUNT = 'after_amount';
    const IS_DEPOSIT = 'is_deposit';
    const TYPE = 'type';
    const TYPE_USER = 'user';
    const TYPE_COMMISSION = 'commission';

    /**
     * @var string[]
     */
    protected $fillable = [
        self::AFTER_AMOUNT,
        self::BEFORE_AMOUNT,
        self::AMOUNT,
        self::WALLET_ID,
        self::IS_DEPOSIT,
        self::TYPE,
    ];

    /**
     * @var array|string[]
     */
    public static array $types = [self::TYPE_USER, self::TYPE_COMMISSION];
}
