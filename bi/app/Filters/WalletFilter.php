<?php

namespace App\Filters;

use Habibi\Filters\Filters;
use Habibi\Traits\Filters\FilterIdsTrait;
use Habibi\Traits\Filters\FilterNameTrait;
use Habibi\Traits\Filters\FilterUserIdTrait;

class WalletFilter extends Filters
{
    use FilterIdsTrait;
    use FilterNameTrait;
    use FilterUserIdTrait;

    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected array $filters = [
        'ids',
        'name',
        'userId',
    ];

    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    public array $attributes = [
        'ids' => 'array',
        'name' => 'string',
        'userId' => 'int',
    ];
}
